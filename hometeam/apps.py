from django.apps import AppConfig


class HometeamConfig(AppConfig):
    name = 'hometeam'
