from django.test import TestCase, Client
from django.urls import reverse
from hometeam.models import Feedback
import json


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.homepage_url = reverse('hometeam:homepage')
        self.team_url = reverse('hometeam:team')

    def test_homepage_GET(self):
        response = self.client.get(self.homepage_url)

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'homepage.html')

    def test_team_GET(self):
        response = self.client.get(self.team_url, { "form":'feedback',"list_feedback": Feedback.objects.all()})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'team.html')
    #

    def test_team_POST_feedback(self):
        response = self.client.post(self.team_url,  { "form":'feedback',"list_feedback": Feedback.objects.all()})
        self.assertEquals(response.status_code, 302)
