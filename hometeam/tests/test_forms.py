from django.test import SimpleTestCase
from hometeam.forms import FeedbackForm

class TestForms(SimpleTestCase):

    def test_form_valid(self):

        form = FeedbackForm(data = {
        'feedback': 'Great Website'
        } )

        self.assertTrue(form.is_valid())

    def test_no_data(self):
        form = FeedbackForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)
