from django.test import TestCase
from hometeam.models import Feedback


class TestModels(TestCase):

    def setUp(self):
        self.project = Feedback.objects.create(
            feedback='Great Website'
        )
        self.project1 = Feedback.objects.create(
            feedback='Great Website'
        )

    def test_project(self):
        self.assertNotEqual(self.project, self.project1 )
