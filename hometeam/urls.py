from django.urls import path
from . import views

app_name = 'hometeam'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('team/', views.team, name='team'),



]
