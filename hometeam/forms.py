from django import forms
from .models import Feedback


class FeedbackForm(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = [
            'feedback'
        ]
        widgets = {
        'feedback': forms.TextInput(attrs={'class':'form-control'}),
        }
