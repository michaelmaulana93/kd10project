from django.db import models
from django.utils.text import slugify


# Create your models here.


class Feedback(models.Model):

    feedback = models.TextField()
    
    def __str__(self):
        return self.feedback
