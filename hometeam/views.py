from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from .models import Feedback
from . import forms



def homepage(request):
    return render(request, 'homepage.html')

def team(request):
    if request.method == "POST" :
        form = forms.FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('hometeam:team')

        return HttpResponse(status=302)
    else:
        form = forms.FeedbackForm()
        return render (request, 'team.html', {"form":form, "list_feedback": Feedback.objects.all()})
