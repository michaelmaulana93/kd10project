[![pipeline](https://gitlab.com/michaelmaulana93/kd10project/badges/master/pipeline.svg)](https://duelbola.herokuapp.com/)

Kelompok    : KD10

Nama        : 

    - Muhammad Michael Maulana (1806191181)
    - Lado Rayhan Najib (1806141265)
    - Habibullah Akbar (1806191351)
    - Luqman Maulana Rizki (1806205092)
        
Link heroku : https://duelbola.herokuapp.com/


Cerita aplikasi yang diajukan serta kebermanfaatannya :
    
    Pencarian lokasi tempat futsal dan informasi tempat futsal tersebut & teman sparing
    Manfaatnya agar lebih mudah mencari tempat futsal yang sesuai keinginan dan dapat teman bermain.
    

Daftar fitur yang akan diimplementasikan :

    - Membuat tim untuk bermain futsal
    - Halaman tempat futsal rekomendasi beserta foto dan deskripsi
    - Mencari tim sparing yang tersedia
    - Form feedback pada halaman kontak